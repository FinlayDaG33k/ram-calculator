import { createAotUrlResolver } from '@angular/compiler';
import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  memoryForm;
  title = 'ram-calculator';
  speedDisplay = 'MHz';
  results = {
    speed: 0,
    pumps: 0,
    transfers: 0,
    width: 0,
    latency: '0',
    channelBandwidth: {
      bits: 0,
      bytes: 0,
      gigabits: 0,
      gigabytes: 0
    },
    totalBandwidth: {
      gigabits: 0,
      gigabytes: 0
    }
  };

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.memoryForm = this.formBuilder.group({
      speed: '2400',
      speedType: 'MT/s',
      memoryType: 'DDR',
      channelWidth: '64',
      channelNumber: '2',
      tcl: '16',
      trcd: '16',
      bl: '4'
    });
  }

  ngOnInit() {
    this.memoryForm.valueChanges.subscribe(val => {
      this.updateSpeedDisplay(val);
      this.calculate(val);
    });

    this.calculate({
      channelNumber: '2',
      channelWidth: '64',
      memoryType: 'DDR',
      speed: '2400',
      speedType: 'MT/s',
      tcl: '16',
      trcd: '16',
      bl: '4'
    });
  }

  updateSpeedDisplay(val: any) {
    switch(val.speedType) {
      case "MT/s":
        this.speedDisplay = "MHz";
        break;
      case "MHz":
        this.speedDisplay = "MT/s";
        break;
      default:
        this.speedDisplay = "???";
        break;
    }
  }

  calculate(values: any): void {
    // Get our pumps based on the memory type
    switch(values.memoryType) {
      case "DDR":
      case "HBM":
        this.results.pumps = 2;
        break;
      case "GDDR":
        this.results.pumps = 4;
        break;
      default:
        return;
    }

    // Calculate out MHz of MT/s
    // Then Calculate transfers per second
    switch(this.speedDisplay){
      case "MT/s":
        this.results.speed = values.speed * this.results.pumps;
        this.results.transfers = values.speed * (this.results.pumps * 1_000_000);
        break;
      case "MHz":
        this.results.speed = values.speed / this.results.pumps;
        this.results.transfers = values.speed * 1_000_000;
        break;
      default:
        return;
    }

    // Calculate the channel width
    this.results.width = values.channelWidth / 8;

    // Calculate the bandwidth per channel
    this.results.channelBandwidth.bits = this.results.transfers * values.channelWidth;
    this.results.channelBandwidth.bytes = this.results.transfers * this.results.width;
    this.results.channelBandwidth.gigabits = this.results.channelBandwidth.bits / 1_000_000_000;
    this.results.channelBandwidth.gigabytes = this.results.channelBandwidth.bytes / 1_000_000_000;

    // Calculate the total bandwidth
    this.results.totalBandwidth.gigabits = this.results.channelBandwidth.gigabits * values.channelNumber;
    this.results.totalBandwidth.gigabytes = this.results.channelBandwidth.gigabytes * values.channelNumber;

    // Calculate first word latency
    // Only do so for DDR
    if(values.memoryType === 'DDR') {
      //this.results.latency = `${(values.cas * 2000) / (this.results.transfers / 1_000_000)} ns`;
      this.results.latency = `${(parseInt(values.trcd) + parseInt(values.tcl) + parseInt(values.bl)) / ((this.results.transfers / 1_000_000) / 2000)} ns`;
    } else {
      this.results.latency = `Not available for ${values.memoryType}`;
    }
  }
}
